/* automatically generated from main_window_ui.xml */
#ifdef __SUNPRO_C
#pragma align 4 (main_window_ui)
#endif
#ifdef __GNUC__
static const char main_window_ui[] __attribute__ ((__aligned__ (4))) =
#else
static const char main_window_ui[] =
#endif
{
  "<ui><menubar name=\"main-menu\"><menu action=\"file-menu\"><menuitem ac"
  "tion=\"open\"/><placeholder name=\"placeholder-open-recent\"/><separato"
  "r/><menuitem action=\"save-copy\"/><separator/><menuitem action=\"prope"
  "rties\"/><separator/><menuitem action=\"close\"/><menuitem action=\"qui"
  "t\"/></menu><menu action=\"edit-menu\"><menu action=\"open-with-menu\">"
  "<placeholder name=\"open-with-apps\" /></menu><separator/><menu action="
  "\"sorting-menu\"><menuitem action=\"sort-filename\"/><menuitem action=\""
  "sort-filetype\"/><menuitem action=\"sort-date\"/></menu><menuitem actio"
  "n=\"delete\"/><separator/><menuitem action=\"clear-private-data\"/><sep"
  "arator/><menuitem action=\"preferences\"/></menu><menu action=\"view-me"
  "nu\"><menuitem action=\"show-toolbar\"/><menuitem action=\"show-statusb"
  "ar\"/><menuitem action=\"show-thumbnailbar\"/><menu action=\"thumbnailb"
  "ar-position-menu\"><menuitem action=\"pos-left\"/><menuitem action=\"po"
  "s-right\"/><menuitem action=\"pos-top\"/><menuitem action=\"pos-bottom\""
  "/></menu><menu action=\"thumbnailbar-size-menu\"><menuitem action=\"siz"
  "e-very-small\"/><menuitem action=\"size-smaller\"/><menuitem action=\"s"
  "ize-small\"/><menuitem action=\"size-normal\"/><menuitem action=\"size-"
  "large\"/><menuitem action=\"size-larger\"/><menuitem action=\"size-very"
  "-large\"/></menu><separator/><menu action=\"zoom-menu\"><menuitem actio"
  "n=\"zoom-in\"/><menuitem action=\"zoom-out\"/><menuitem action=\"zoom-f"
  "it\"/><menuitem action=\"zoom-100\"/></menu><menu action=\"rotation-men"
  "u\"><menuitem action=\"rotate-cw\"/><menuitem action=\"rotate-ccw\"/></"
  "menu><menu action=\"flip-menu\"><menuitem action=\"flip-horizontally\"/"
  "><menuitem action=\"flip-vertically\"/></menu><separator/><menuitem act"
  "ion=\"fullscreen\"/><separator/><menuitem action=\"set-as-wallpaper\"/>"
  "</menu><menu action=\"go-menu\"><menuitem action=\"back\"/><menuitem ac"
  "tion=\"forward\"/><menuitem action=\"first\"/><menuitem action=\"last\""
  "/><separator/><placeholder name=\"placeholder-slideshow\" /></menu><men"
  "u action=\"help-menu\"><menuitem action=\"contents\"/><menuitem action="
  "\"about\"/></menu></menubar><popup name=\"navigation-toolbar-menu\"><me"
  "nu action=\"position-menu\"><menuitem action=\"pos-left\"/><menuitem ac"
  "tion=\"pos-right\"/><menuitem action=\"pos-top\"/><menuitem action=\"po"
  "s-bottom\"/></menu><menu action=\"size-menu\"><menuitem action=\"size-v"
  "ery-small\"/><menuitem action=\"size-smaller\"/><menuitem action=\"size"
  "-small\"/><menuitem action=\"size-normal\"/><menuitem action=\"size-lar"
  "ge\"/><menuitem action=\"size-larger\"/><menuitem action=\"size-very-la"
  "rge\"/></menu></popup><popup name=\"image-viewer-menu\"><menuitem actio"
  "n=\"open\"/><menuitem action=\"properties\"/><menuitem action=\"close\""
  "/><separator/><menu action=\"open-with-menu\"><placeholder name=\"open-"
  "with-apps\" /></menu><separator/><menuitem action=\"zoom-in\"/><menuite"
  "m action=\"zoom-out\"/><menuitem action=\"zoom-100\"/><menuitem action="
  "\"zoom-fit\"/></popup><toolbar name=\"main-toolbar\"><toolitem action=\""
  "open\"/><separator /><toolitem action=\"save-copy\"/><toolitem action=\""
  "delete\"/><separator /><toolitem action=\"edit\"/><separator name=\"sep"
  "arator-1\"/><toolitem action=\"back\"/><placeholder name=\"placeholder-"
  "slideshow\" /><toolitem action=\"forward\"/><separator name=\"separator"
  "-2\"/><toolitem action=\"zoom-in\"/><toolitem action=\"zoom-out\"/><too"
  "litem action=\"zoom-100\"/><toolitem action=\"zoom-fit\"/><separator />"
  "<placeholder name=\"placeholder-fullscreen\" /></toolbar></ui>"
};

static const unsigned main_window_ui_length = 3282u;

