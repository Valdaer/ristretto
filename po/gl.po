# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Daniel Muñiz Fontoira <dani@damufo.com>, 2018-2019
# Leandro Regueiro <leandro DOT regueiro AT gmail DOT com>, 2008-2010
# Leandro Regueiro <leandro.regueiro@gmail.com>, 2013
# Xosé, 2017
msgid ""
msgstr ""
"Project-Id-Version: Xfce Apps\n"
"Report-Msgid-Bugs-To: xfce-i18n@xfce.org\n"
"POT-Creation-Date: 2019-07-13 00:31+0200\n"
"PO-Revision-Date: 2019-07-15 08:45+0000\n"
"Last-Translator: Daniel Muñiz Fontoira <dani@damufo.com>\n"
"Language-Team: Galician (http://www.transifex.com/xfce/xfce-apps/language/gl/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: gl\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ../src/main.c:62
msgid "Version information"
msgstr "Información de versión"

#: ../src/main.c:66
msgid "Start in fullscreen mode"
msgstr "Iniciar en modo de pantalla completa"

#: ../src/main.c:70
msgid "Start a slideshow"
msgstr "Iniciar unha presentación"

#: ../src/main.c:78
msgid "Show settings dialog"
msgstr "Amosar o diálogo de configuración"

#: ../src/main.c:108
#, c-format
msgid ""
"%s: %s\n"
"\n"
"Try %s --help to see a full list of\n"
"available command line options.\n"
msgstr "%s: %s\nExecute %s --help para ver unha lista completa das opcións de liña de ordes dispoñibles.\n"

#: ../src/main_window.c:53 ../ristretto.desktop.in.h:3
msgid "Image Viewer"
msgstr "Visor de imaxes"

#: ../src/main_window.c:371
msgid "_File"
msgstr "_Ficheiro"

#. Icon-name
#: ../src/main_window.c:377
msgid "_Open..."
msgstr "_Abrir..."

#. Label-text
#. Keyboard shortcut
#: ../src/main_window.c:379
msgid "Open an image"
msgstr "Abrir unha imaxe"

#. Icon-name
#: ../src/main_window.c:383
msgid "_Save copy..."
msgstr "Gar_dar unha copia..."

#. Label-text
#. Keyboard shortcut
#: ../src/main_window.c:385
msgid "Save a copy of the image"
msgstr "Gardar unha copia da imaxe"

#. Icon-name
#: ../src/main_window.c:389
msgid "_Properties..."
msgstr "_Propiedades..."

#. Label-text
#. Keyboard shortcut
#: ../src/main_window.c:391
msgid "Show file properties"
msgstr "Amosar as propiedades do ficheiro"

#. Icon-name
#: ../src/main_window.c:395 ../src/main_window.c:414
msgid "_Edit"
msgstr "_Editar"

#. Label-text
#. Keyboard shortcut
#: ../src/main_window.c:397
msgid "Edit this image"
msgstr "Editar esta imaxe"

#. Icon-name
#: ../src/main_window.c:401 ../src/preferences_dialog.c:541
#: ../src/properties_dialog.c:325
msgid "_Close"
msgstr "_Pechar"

#. Label-text
#. Keyboard shortcut
#: ../src/main_window.c:403
msgid "Close this image"
msgstr "Pechar esta imaxe"

#. Icon-name
#: ../src/main_window.c:407
msgid "_Quit"
msgstr "_Saír"

#. Label-text
#. Keyboard shortcut
#: ../src/main_window.c:409
msgid "Quit Ristretto"
msgstr "Saír de Ristretto"

#: ../src/main_window.c:420
msgid "_Open with"
msgstr "_Abrir con"

#: ../src/main_window.c:426
msgid "_Sort by"
msgstr "_Ordenar por"

#. Icon-name
#: ../src/main_window.c:432
msgid "_Delete"
msgstr "_Eliminar"

#. Label-text
#. Keyboard shortcut
#: ../src/main_window.c:434
msgid "Delete this image from disk"
msgstr "Eliminar esta imaxe do disco"

#. Icon-name
#: ../src/main_window.c:438
msgid "_Clear private data..."
msgstr "_Limpar os datos privados..."

#. Icon-name
#: ../src/main_window.c:444
msgid "_Preferences..."
msgstr "_Preferencias..."

#: ../src/main_window.c:451
msgid "_View"
msgstr "_Ver"

#. Icon-name
#: ../src/main_window.c:457
msgid "_Fullscreen"
msgstr "_Pantalla completa"

#. Label-text
#. Keyboard shortcut
#: ../src/main_window.c:459
msgid "Switch to fullscreen"
msgstr "Cambiar a modo de pantalla completa"

#. Icon-name
#: ../src/main_window.c:463
msgid "_Leave Fullscreen"
msgstr "_Saír do modo de pantalla completa"

#. Label-text
#. Keyboard shortcut
#: ../src/main_window.c:465
msgid "Leave Fullscreen"
msgstr "Saír do modo de pantalla completa"

#. Icon-name
#: ../src/main_window.c:469
msgid "Set as _Wallpaper..."
msgstr "Definir como fondo de escritorio"

#: ../src/main_window.c:476
msgid "_Zoom"
msgstr "_Ampliación"

#. Icon-name
#: ../src/main_window.c:482
msgid "Zoom _In"
msgstr "_Aumentar"

#. Label-text
#. Keyboard shortcut
#: ../src/main_window.c:484
msgid "Zoom in"
msgstr "Aumentar"

#. Icon-name
#: ../src/main_window.c:488
msgid "Zoom _Out"
msgstr "_Reducir"

#. Label-text
#. Keyboard shortcut
#: ../src/main_window.c:490
msgid "Zoom out"
msgstr "Reducir"

#. Icon-name
#: ../src/main_window.c:494
msgid "Zoom _Fit"
msgstr "_Encaixar"

#. Label-text
#. Keyboard shortcut
#: ../src/main_window.c:496
msgid "Zoom to fit window"
msgstr "Axustar á xanela"

#. Icon-name
#: ../src/main_window.c:500
msgid "_Normal Size"
msgstr "Tamaño _normal"

#. Label-text
#. Keyboard shortcut
#: ../src/main_window.c:502
msgid "Zoom to 100%"
msgstr "Axustar ao 100%"

#: ../src/main_window.c:507
msgid "_Rotation"
msgstr "_Rotación"

#. Icon-name
#: ../src/main_window.c:513
msgid "Rotate _Right"
msgstr "Rotar á _dereita"

#. Icon-name
#: ../src/main_window.c:519
msgid "Rotate _Left"
msgstr "Rotar á _esqueda"

#: ../src/main_window.c:526
msgid "_Flip"
msgstr "_Voltear"

#: ../src/main_window.c:532
msgid "Flip _Horizontally"
msgstr "Voltear na _horizontal"

#: ../src/main_window.c:538
msgid "Flip _Vertically"
msgstr "Voltear na _vertical"

#: ../src/main_window.c:545
msgid "_Go"
msgstr "_Ir"

#. Icon-name
#: ../src/main_window.c:551
msgid "_Forward"
msgstr "_Seguinte"

#. Label-text
#. Keyboard shortcut
#: ../src/main_window.c:553
msgid "Next image"
msgstr "Seguinte imaxe"

#. Icon-name
#: ../src/main_window.c:557
msgid "_Back"
msgstr "_Atrás"

#. Label-text
#. Keyboard shortcut
#: ../src/main_window.c:559
msgid "Previous image"
msgstr "Imaxe anterior"

#. Icon-name
#: ../src/main_window.c:563
msgid "F_irst"
msgstr "_Primeira"

#. Label-text
#. Keyboard shortcut
#: ../src/main_window.c:565
msgid "First image"
msgstr "Primeira imaxe"

#. Icon-name
#: ../src/main_window.c:569
msgid "_Last"
msgstr "Ú_ltima"

#. Label-text
#. Keyboard shortcut
#: ../src/main_window.c:571
msgid "Last image"
msgstr "Última imaxe"

#: ../src/main_window.c:576
msgid "_Help"
msgstr "A_xuda"

#. Icon-name
#: ../src/main_window.c:582
msgid "_Contents"
msgstr "_Contido"

#. Label-text
#. Keyboard shortcut
#: ../src/main_window.c:584
msgid "Display ristretto user manual"
msgstr "Amosar o manual de usuario de Ristretto"

#. Icon-name
#: ../src/main_window.c:588
msgid "_About"
msgstr "_Acerca de"

#. Label-text
#. Keyboard shortcut
#: ../src/main_window.c:590
msgid "Display information about ristretto"
msgstr "Amosar información acerca de Ristretto"

#: ../src/main_window.c:595
msgid "_Position"
msgstr "_Posición"

#: ../src/main_window.c:601
msgid "_Size"
msgstr "_Tamaño"

#: ../src/main_window.c:607
msgid "Thumbnail Bar _Position"
msgstr "_Posición da barra de miniaturas"

#: ../src/main_window.c:613
msgid "Thumb_nail Size"
msgstr "Tamaño das mi_niaturas"

#. Icon-name
#: ../src/main_window.c:620
msgid "Leave _Fullscreen"
msgstr "_Saír do modo de pantalla completa"

#. Icon-name
#: ../src/main_window.c:638
msgid "_Show Toolbar"
msgstr "Mo_strar a barra de ferramentas"

#. Icon-name
#: ../src/main_window.c:646
msgid "Show _Thumbnail Bar"
msgstr "Mos_trar a barra de miniaturas"

#. Icon-name
#: ../src/main_window.c:654
msgid "Show Status _Bar"
msgstr "Amosar a _barra de estado"

#. Icon-name
#: ../src/main_window.c:666
msgid "file name"
msgstr "nome de ficheiro"

#. Icon-name
#: ../src/main_window.c:672
msgid "file type"
msgstr "tipo de ficheiro"

#. Icon-name
#: ../src/main_window.c:678
msgid "date"
msgstr "data"

#. Icon-name
#: ../src/main_window.c:689
msgid "Left"
msgstr "Esquerda"

#: ../src/main_window.c:695
msgid "Right"
msgstr "Dereita"

#: ../src/main_window.c:701
msgid "Top"
msgstr "Superior"

#: ../src/main_window.c:707
msgid "Bottom"
msgstr "Inferior"

#: ../src/main_window.c:718
msgid "Very Small"
msgstr "Moi pequeno"

#: ../src/main_window.c:724
msgid "Smaller"
msgstr "Bastante pequeno"

#: ../src/main_window.c:730
msgid "Small"
msgstr "Pequeno"

#: ../src/main_window.c:736
msgid "Normal"
msgstr "Normal"

#: ../src/main_window.c:742
msgid "Large"
msgstr "Grande"

#: ../src/main_window.c:748
msgid "Larger"
msgstr "Bastante grande"

#: ../src/main_window.c:754
msgid "Very Large"
msgstr "Moi grande"

#. Create Play/Pause Slideshow actions
#: ../src/main_window.c:909
msgid "_Play"
msgstr "_Iniciar"

#: ../src/main_window.c:909
msgid "Play slideshow"
msgstr "Iniciar a presentación"

#: ../src/main_window.c:910
msgid "_Pause"
msgstr "_Pausar"

#: ../src/main_window.c:910
msgid "Pause slideshow"
msgstr "Pausar a presentación"

#. Create Recently used items Action
#: ../src/main_window.c:913
msgid "_Recently used"
msgstr "Usados _recentemente"

#: ../src/main_window.c:913
msgid "Recently used"
msgstr "Usados recentemente"

#: ../src/main_window.c:1017 ../src/main_window.c:1604
msgid "Press open to select an image"
msgstr "Prema abrir para seleccionar unha imaxe"

#: ../src/main_window.c:1453 ../src/main_window.c:1457
msgid "Open With Other _Application..."
msgstr "Abrir con outro _aplicativo..."

#: ../src/main_window.c:1478 ../src/main_window.c:1484
msgid "Empty"
msgstr "Baleiro"

#: ../src/main_window.c:1614
msgid "Loading..."
msgstr "Cargando..."

#: ../src/main_window.c:2221
msgid "Choose 'set wallpaper' method"
msgstr "Escoller o método de «definición de fondo de escritorio»"

#: ../src/main_window.c:2224 ../src/main_window.c:4032
#: ../src/xfce_wallpaper_manager.c:434 ../src/gnome_wallpaper_manager.c:241
msgid "_OK"
msgstr "_Aceptar"

#: ../src/main_window.c:2226 ../src/main_window.c:3161
#: ../src/main_window.c:3395 ../src/main_window.c:4030
#: ../src/privacy_dialog.c:180 ../src/xfce_wallpaper_manager.c:430
#: ../src/gnome_wallpaper_manager.c:237
msgid "_Cancel"
msgstr "_Cancelar"

#: ../src/main_window.c:2236 ../src/preferences_dialog.c:462
msgid ""
"Configure which system is currently managing your desktop.\n"
"This setting determines the method <i>Ristretto</i> will use\n"
"to configure the desktop wallpaper."
msgstr "Configure que sistema está xestionando o escritorio.\nEsta opción determina o método que empregará\n<i>Ristretto</i> para configurar o fondo de escritorio."

#: ../src/main_window.c:2261 ../src/preferences_dialog.c:486
msgid "None"
msgstr "Ningún"

#: ../src/main_window.c:2265 ../src/preferences_dialog.c:490
msgid "Xfce"
msgstr "Xfce"

#: ../src/main_window.c:2269 ../src/preferences_dialog.c:494
msgid "GNOME"
msgstr "GNOME"

#: ../src/main_window.c:2722
msgid "Developer:"
msgstr "Desenvolvedor:"

#: ../src/main_window.c:2731 ../ristretto.appdata.xml.in.h:1
msgid "Ristretto is an image viewer for the Xfce desktop environment."
msgstr "Ristretto é un visor de imaxes para o contorno de escritorio Xfce."

#: ../src/main_window.c:2739
msgid "translator-credits"
msgstr "Leandro Regueiro <leandro.regueiro@gmail.com>, 2008, 2009, 2010\n\nProxecto Trasno http://trasno.net"

#: ../src/main_window.c:3158
msgid "Open image"
msgstr "Abrir imaxe"

#: ../src/main_window.c:3162
msgid "_Open"
msgstr "_Abrir"

#: ../src/main_window.c:3179
msgid "Images"
msgstr "Imaxes"

#: ../src/main_window.c:3184
msgid ".jp(e)g"
msgstr ".jp(e)g"

#: ../src/main_window.c:3211 ../src/main_window.c:3353
msgid "Could not open file"
msgstr "Non se puido abrir o ficheiro"

#: ../src/main_window.c:3392
msgid "Save copy"
msgstr "Gardar unha copia"

#: ../src/main_window.c:3396
msgid "_Save"
msgstr "_Gardar"

#: ../src/main_window.c:3425
msgid "Could not save file"
msgstr "Non se puido gardar o ficheiro"

#: ../src/main_window.c:3595
#, c-format
msgid "Are you sure you want to send image '%s' to trash?"
msgstr "Está seguro de que desexa enviar a imaxe \"%s\" ao colector de lixo?"

#: ../src/main_window.c:3599
#, c-format
msgid "Are you sure you want to delete image '%s' from disk?"
msgstr "Está seguro de que desexa eliminar a imaxe \"%s\" do disco?"

#: ../src/main_window.c:3610
msgid "_Do not ask again for this session"
msgstr "_Non preguntar máis nesta sesión"

#: ../src/main_window.c:3687
#, c-format
msgid ""
"An error occurred when deleting image '%s' from disk.\n"
"\n"
"%s"
msgstr "Produciuse un erro ao eliminar a imaxe '%s' do disco.\n\n%s"

#: ../src/main_window.c:3691
#, c-format
msgid ""
"An error occurred when sending image '%s' to trash.\n"
"\n"
"%s"
msgstr "Produciuse un erro ao enviar a imaxe '%s' ao lixo.\n\n%s"

#: ../src/main_window.c:4027
msgid "Edit with"
msgstr "Editar con"

#: ../src/main_window.c:4045
#, c-format
msgid "Open %s and other files of type %s with:"
msgstr "Abrir %s e outros ficheiros de tipo %s con:"

#: ../src/main_window.c:4051
msgid "Use as _default for this kind of file"
msgstr "Usar como pre_determinado para este tipo de ficheiro"

#: ../src/main_window.c:4141
msgid "Recommended Applications"
msgstr "Aplicativos recomendados"

#: ../src/main_window.c:4221
msgid "Other Applications"
msgstr "Outros aplicativos"

#: ../src/icon_bar.c:345
msgid "Orientation"
msgstr "_Orientación"

#: ../src/icon_bar.c:346
msgid "The orientation of the iconbar"
msgstr "A orientación da barra de iconas"

#: ../src/icon_bar.c:362
msgid "File column"
msgstr "Columna de ficheiro"

#: ../src/icon_bar.c:363
msgid "Model column used to retrieve the file from"
msgstr "Columna modelo que se emprega para obter o ficheiro"

#: ../src/icon_bar.c:375
msgid "Icon Bar Model"
msgstr "Modelo da barra de iconas"

#: ../src/icon_bar.c:376
msgid "Model for the icon bar"
msgstr "Modelo da barra de iconas"

#: ../src/icon_bar.c:392
msgid "Active"
msgstr "Activo"

#: ../src/icon_bar.c:393
msgid "Active item index"
msgstr "Índice do elemento activo"

#: ../src/icon_bar.c:409 ../src/icon_bar.c:410
msgid "Show Text"
msgstr "Amosar o texto"

#: ../src/icon_bar.c:416 ../src/icon_bar.c:417
msgid "Active item fill color"
msgstr "Cor de recheo dos elementos activos"

#: ../src/icon_bar.c:423 ../src/icon_bar.c:424
msgid "Active item border color"
msgstr "Cor do bordo dos elementos activos"

#: ../src/icon_bar.c:430 ../src/icon_bar.c:431
msgid "Active item text color"
msgstr "Cor do texto dos elementos activos"

#: ../src/icon_bar.c:437 ../src/icon_bar.c:438
msgid "Cursor item fill color"
msgstr "Cor de recheo do elemento baixo o cursor"

#: ../src/icon_bar.c:444 ../src/icon_bar.c:445
msgid "Cursor item border color"
msgstr "Cor do bordo do elemento baixo o cursor"

#: ../src/icon_bar.c:451 ../src/icon_bar.c:452
msgid "Cursor item text color"
msgstr "Cor do texto do elemento baixo o cursor"

#: ../src/privacy_dialog.c:150
msgid "Time range to clear:"
msgstr "Intervalo de tempo a limpar:"

#: ../src/privacy_dialog.c:154
msgid "Cleanup"
msgstr "Limpeza"

#: ../src/privacy_dialog.c:156
msgid "Last Hour"
msgstr "Última hora"

#: ../src/privacy_dialog.c:157
msgid "Last Two Hours"
msgstr "Últimas dúas horas"

#: ../src/privacy_dialog.c:158
msgid "Last Four Hours"
msgstr "Últimas catro horas"

#: ../src/privacy_dialog.c:159
msgid "Today"
msgstr "Hoxe"

#: ../src/privacy_dialog.c:160
msgid "Everything"
msgstr "Todo"

#: ../src/privacy_dialog.c:181 ../src/xfce_wallpaper_manager.c:432
#: ../src/gnome_wallpaper_manager.c:239
msgid "_Apply"
msgstr "_Aplicar"

#: ../src/privacy_dialog.c:469
msgid "Clear private data"
msgstr "Limpar os datos privados"

#: ../src/preferences_dialog.c:274
msgid "Display"
msgstr "Pantalla"

#: ../src/preferences_dialog.c:281
msgid "Background color"
msgstr "Cor de fondo"

#: ../src/preferences_dialog.c:285
msgid "Override background color:"
msgstr "Substituír a cor de fondo:"

#: ../src/preferences_dialog.c:312
msgid "Quality"
msgstr "Calidade"

#: ../src/preferences_dialog.c:316
msgid ""
"With this option enabled, the maximum image-quality will be limited to the "
"screen-size."
msgstr "Con esta opción activada, a calidade de imaxe máxima limitarase ao tamaño da pantalla."

#: ../src/preferences_dialog.c:319
msgid "Limit rendering quality"
msgstr "Limitar a calidade de procesado"

#: ../src/preferences_dialog.c:331
msgid "Fullscreen"
msgstr "Pantalla completa"

#: ../src/preferences_dialog.c:336
msgid "Thumbnails"
msgstr "Miniaturas"

#: ../src/preferences_dialog.c:339
msgid ""
"The thumbnail bar can be automatically hidden when the window is fullscreen."
msgstr "Pode agochar automaticamente a barra de miniaturas cando a xanela está a pantalla completa."

#: ../src/preferences_dialog.c:342
msgid "Hide thumbnail bar when fullscreen"
msgstr "Agochar a barra de miniaturas cando se estea a pantalla completa"

#: ../src/preferences_dialog.c:350
msgid "Clock"
msgstr "Reloxo"

#: ../src/preferences_dialog.c:353
msgid ""
"Show an analog clock that displays the current time when the window is "
"fullscreen"
msgstr "Amosar un reloxo analóxico coa a hora actual cando a xanela esta a pantalla completa"

#: ../src/preferences_dialog.c:356
msgid "Show Fullscreen Clock"
msgstr "Amosar reloxo a pantalla completa"

#: ../src/preferences_dialog.c:373
msgid "Slideshow"
msgstr "Presentación"

#: ../src/preferences_dialog.c:377
msgid "Timeout"
msgstr "Tempo de espera"

#: ../src/preferences_dialog.c:380
msgid ""
"The time period an individual image is displayed during a slideshow\n"
"(in seconds)"
msgstr "O período de tempo que se amosa cada imaxe durante unha\npresentación (en segundos)"

#: ../src/preferences_dialog.c:398
msgid "Control"
msgstr "Control"

#: ../src/preferences_dialog.c:402
msgid "Scroll wheel"
msgstr "Roda de desprazamento"

#: ../src/preferences_dialog.c:405
msgid "Invert zoom direction"
msgstr "Inverter a dirección do zoom"

#: ../src/preferences_dialog.c:419
msgid "Behaviour"
msgstr "Comportamento"

#: ../src/preferences_dialog.c:424
msgid "Startup"
msgstr "Inicio"

#: ../src/preferences_dialog.c:426
msgid "Maximize window on startup when opening an image"
msgstr "Maximizar a xanela cando se inicie para ver unha imaxe"

#: ../src/preferences_dialog.c:432
msgid "Wrap around images"
msgstr "Envoltorio arredor das imaxes"

#: ../src/preferences_dialog.c:451
msgid "Desktop"
msgstr "Escritorio"

#: ../src/preferences_dialog.c:576
msgid "Image Viewer Preferences"
msgstr "Preferencias do visor de imaxes"

#: ../src/properties_dialog.c:178
msgid "<b>Name:</b>"
msgstr "<b>Nome:</b>"

#: ../src/properties_dialog.c:179
msgid "<b>Kind:</b>"
msgstr "<b>Clase:</b>"

#: ../src/properties_dialog.c:180
msgid "<b>Modified:</b>"
msgstr "<b>Modificación:</b>"

#: ../src/properties_dialog.c:181
msgid "<b>Accessed:</b>"
msgstr "<b>Acceso:</b>"

#: ../src/properties_dialog.c:182
msgid "<b>Size:</b>"
msgstr "<b>Tamaño:</b>"

#: ../src/properties_dialog.c:306
msgid "General"
msgstr "Xeral"

#: ../src/properties_dialog.c:310
msgid "Image"
msgstr "Imaxe"

#: ../src/properties_dialog.c:545
#, c-format
msgid "<b>Date taken:</b>"
msgstr "<b>Data de creación:</b>"

#: ../src/properties_dialog.c:557 ../src/properties_dialog.c:569
#: ../src/properties_dialog.c:581
#, c-format
msgid "<b>%s</b>"
msgstr "<b>%s</b>"

#: ../src/properties_dialog.c:647
#, c-format
msgid "%s - Properties"
msgstr "%s - Propiedades"

#: ../src/thumbnailer.c:439
msgid ""
"The thumbnailer-service can not be reached,\n"
"for this reason, the thumbnails can not be\n"
"created.\n"
"\n"
"Install <b>Tumbler</b> or another <i>thumbnailing daemon</i>\n"
"to resolve this issue."
msgstr "Non se pode contactar con thumbnailer-service,\ne debido a esta razón non se poden\ncrear as miniaturas.\n\nInstale <b>Tumbler</b> ou outro <i>daemon de miniaturas</i>\npara resolver esta incidencia."

#: ../src/thumbnailer.c:449
msgid "Do _not show this message again"
msgstr "_Non volver amosar esta mensaxe"

#: ../src/xfce_wallpaper_manager.c:409 ../src/gnome_wallpaper_manager.c:223
msgid "Style:"
msgstr "Estilo:"

#: ../src/xfce_wallpaper_manager.c:420
msgid "Apply to all workspaces"
msgstr "Aplicar a todos os espazos de traballo"

#: ../src/xfce_wallpaper_manager.c:427 ../src/gnome_wallpaper_manager.c:234
msgid "Set as wallpaper"
msgstr "Definir como fondo de escritorio"

#: ../src/xfce_wallpaper_manager.c:495 ../src/gnome_wallpaper_manager.c:301
msgid "Auto"
msgstr "Automático"

#: ../src/xfce_wallpaper_manager.c:498
msgid "Centered"
msgstr "Centrado"

#: ../src/xfce_wallpaper_manager.c:501
msgid "Tiled"
msgstr "Mosaico"

#: ../src/xfce_wallpaper_manager.c:504
msgid "Stretched"
msgstr "Estirada"

#: ../src/xfce_wallpaper_manager.c:507
msgid "Scaled"
msgstr "Escalada"

#: ../src/xfce_wallpaper_manager.c:510
msgid "Zoomed"
msgstr "Ampliada"

#: ../ristretto.desktop.in.h:1
msgid "Ristretto Image Viewer"
msgstr "Visor de imaxes Ristretto"

#: ../ristretto.desktop.in.h:2
msgid "Look at your images easily"
msgstr "Mire as súas imaxes sen complicarse"

#: ../ristretto.appdata.xml.in.h:2
msgid ""
"The Ristretto Image Viewer is an application that can be used to view and "
"scroll through images, run a slideshow of images, open images with other "
"applications like an image-editor or configure an image as the desktop "
"wallpaper."
msgstr "O Visor de Imaxes Ristretto é un aplicativo que se pode empregar para ver imaxes, reproducir unha presentación de imaxes, abrir imaxes con outros aplicativos, como un editor de imaxes, ou configurar unha imaxe como fondo de pantalla."
